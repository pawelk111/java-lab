package pl.pk.edu.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class ComponentTest {

    @Test
    public void getName() {
        Component component = new Component("balsam", 3);
        assertEquals("balsam", component.getName());
    }

    @Test
    public void getCount() {
        Component component = new Component("Oliwa", 10);
        assertEquals(10, component.getCount());
    }
}