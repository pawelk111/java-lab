package pl.pk.edu.model;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class CookTest {

    @Test
    public void getName() {
        Cook cook = new Cook("ala", null);
        assertEquals(cook.getName(),"ala");
    }

    @Test
    public void setName() {
        Cook cook = new Cook("ala", null);
        cook.setName("Orzechy");
        assertEquals(cook.getName(),"Orzechy");
    }

    @Test
    public void getComponentList() {
        Component component = new Component("aa", 2);

        Cook cook = new Cook("ala", Arrays.asList(component));
        assertEquals(component,cook.getComponentList().get(0));
    }

    @Test
    public void setComponentList() {
        Cook cook = new Cook("ala", null);
        Component component = new Component("aa", 2);

        cook.setComponentList(Arrays.asList(component));
        assertEquals(cook.getComponentList().get(0), component);
    }
}