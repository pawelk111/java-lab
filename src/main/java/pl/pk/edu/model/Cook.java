package pl.pk.edu.model;

import java.util.List;

public class Cook {

    private String name;
    private List<Component> componentList;

    public Cook(String name, List<Component> componentList) {
        this.name = name;
        this.componentList = componentList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Component> getComponentList() {
        return componentList;
    }

    public void setComponentList(List<Component> componentList) {
        this.componentList = componentList;
    }

    @Override
    public String toString() {
        return "Cook{" +
                "name='" + name + '\'' +
                ", componentList=" + componentList +
                '}';
    }
}
