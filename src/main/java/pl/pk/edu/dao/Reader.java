package pl.pk.edu.dao;

import pl.pk.edu.model.Cook;

import java.util.List;

public interface Reader {

    List<Cook> readData(String fileName);
}
