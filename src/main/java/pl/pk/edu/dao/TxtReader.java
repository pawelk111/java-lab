package pl.pk.edu.dao;

import pl.pk.edu.model.Component;
import pl.pk.edu.model.Cook;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class TxtReader implements Reader {
    @Override
    public List<Cook> readData(String fileName) {

        List<Cook> cookList = new ArrayList<>();

        try {
            File file = new File("src/main/resources/cookbook.txt");
            Scanner scanner = new Scanner(file);

            while(scanner.hasNextLine()) {
                String cookAsString  = scanner.nextLine();
                String cookName = cookAsString.split(",")[0];
                String componentName = cookAsString.split(",")[1].split(",")[0];
                String componentCount = cookAsString.split(",")[1]
                        .replace("[","")
                        .replace("]","")
                        .split(";")[1];

                List<Component> components = new ArrayList<>();
                components.add(new Component(componentName,Integer.valueOf(componentCount)));

                Cook cook = new Cook(cookName, components);
                cookList.add(cook);
            }
            return cookList;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
