package pl.pk.edu;

import pl.pk.edu.controller.MainController;
import pl.pk.edu.model.Component;
import pl.pk.edu.model.Cook;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

        MainController mainController = new MainController();
        mainController.initController();
//        mainController.addCook(cook);
        mainController.showRecepies();
    }
}
