package pl.pk.edu.view;

import pl.pk.edu.model.Component;
import pl.pk.edu.model.Cook;

public class ProfessionalPrinter extends Printer {

    @Override
    public void printCook(Cook cook) {
        String cookAsString = cook.getName() + "\n Produkty: \n";

        for(Component component : cook.getComponentList()) {
            cookAsString += "----Nazwa:" +  component.getName() + "----ilosc" + component.getCount() + "\n";
        }
        System.out.println(cookAsString);
    }
}
