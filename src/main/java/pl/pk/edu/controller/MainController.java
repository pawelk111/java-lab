package pl.pk.edu.controller;

import pl.pk.edu.dao.Reader;
import pl.pk.edu.dao.TxtReader;
import pl.pk.edu.model.Cook;
import pl.pk.edu.view.Printer;
import pl.pk.edu.view.ProfessionalPrinter;

import java.util.ArrayList;
import java.util.List;

public class MainController {

    private Printer printer;
    private List<Cook> cookList;
    private Reader reader;

    public void initController() {
        printer = new ProfessionalPrinter();
        cookList = new ArrayList<>();
        reader = new TxtReader();
        readRecepies();
    }

    private void readRecepies() {
        cookList = reader.readData("src/main/resources/cookbook.txt");
    }

    public void showRecepies() {
        for(Cook cook : cookList) {
            printer.printCook(cook);
        }
    }

    public void addCook(Cook cook) {
        cookList.add(cook);
    }

    public Cook findBook(String name) {
        for(Cook cook : cookList) {
            if(cook.getName().equals(name)) {
                return cook;
            }
        }
        return null;
    }
}
